//
//  WaveTableOsc.cpp
//  NoiseWavetables
//
//  Created by lxinspc on 30/03/2022.
//

#include "WaveTableOsc.h"

WaveTableOsc::WaveTableOsc() {
  
  //Make sure we have a an empty vector of sample values
  _waveTable.resize(_internal_size);
  
}

void WaveTableOsc::setup(double sampleRate) {
  
  _sampleRate = sampleRate;
  
}

void WaveTableOsc::set(WaveTable wt, bool addSamples) {
  
  _waveTable = wt;
  
  //in the sinbu4 implentation, the buffer is 2 sizes larger than the sample length - with the first two
  //values repeated, this saves on some math when reaing the last samples, as we can look ahead by
  //two values without having to check and calcuate the beginning positions. We can achieve this by taking the
  //sample we recieved, and copying the first and last samples to the end

  if (addSamples) {
    _waveTable.push_back(_waveTable[0]);
    _waveTable.push_back(_waveTable[1]);
  }

  
  _size = _waveTable.size() - 2;
  
}

double WaveTableOsc::getSample(double frequency) {
  
  //This is exactly the same code as maxiOsc::sinebuf4 - except rather than use
  //sine buf - we use the provided wave table buffer instead
  //some changes made for code style and the actual 4 point interpolation is moved into a static
  //function as may use else where - for example interpolating between the wt start and end

  _phase += _size/(_sampleRate/(frequency));
  if ( _phase >= _size - 1 ) {
    _phase -=_size;
  }
  double remainder = _phase - floor(_phase);

  long aPos = ( _phase == 0 ) ? _size : _phase - 1;

  return interpolate4(_waveTable[aPos],
                      _waveTable[_phase],
                      _waveTable[_phase + 1],
                      _waveTable[_phase + 2],
                      remainder);
  
}

double WaveTableOsc::interpolate4(double a, double b, double c, double d, double remainder) {
  
  //Straight from the sinbuf4 implmentation - some changes for style
  
  double a1 = 0.5 * (c - a);
  double a2 = a - 2.5 * b + 2.0 * c - 0.5 * d;
  double a3 = 0.5 * ( d - a ) + 1.5 * ( b - c );

  double output = (( a3 * remainder + a2 ) * remainder + a1 ) * remainder + b;
  
  return output;
    
}





