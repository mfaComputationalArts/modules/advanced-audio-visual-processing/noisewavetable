/**
 
 Class: WaveTableOsc

 
 
 This is based on maxiOsc sinebuf4 which used 4 point interpolation on a buffer of a sinewave
 the difference here is that we can load out own data into the buffer.
 
 Should support multiple sample lengths, but not yet tested at anything other than 512 samples
 
 */


#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add on includes
#include "maximilian.h"

//Local includes


class WaveTableOsc {
  
public:
  
  WaveTableOsc();
  
  typedef vector<double> WaveTable;
  
  void setup(double sampleRate);
  
  //Set new wavetable data - addSamples used to add first two samples onto the end of wt
  void set(WaveTable wt, bool addSamples = true);
 
  //Get the next single sample from the WaveTable
  double getSample(double frequency);
  
  //Utility method for 4 point interpolation
  static double interpolate4(double a, double b, double c, double d, double remainder);
  
private:
  
  //Wave table size - defaults to 512, but is updated in setWaveTable
  size_t _size = 512;
  size_t _internal_size = _size + 2;
  
  double _sampleRate;
  
  //Keep track of current position
  double _phase;
  
  //The actual wavetable
  WaveTable _waveTable;
  
};

