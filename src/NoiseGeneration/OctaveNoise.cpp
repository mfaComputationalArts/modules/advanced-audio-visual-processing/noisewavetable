//
//  OctaveNoise.cpp
//  NoiseWavetables
//
//  Created by lxinspc on 28/03/2022.
//

#include "OctaveNoise.h"

void OctaveNoise::setup(Options o) {
  
  ofBackground(5,5,5);
  
  //Setup the wave table oscilator
  _waveTableOsc.setup(o.sampleRate);
  
  //Setup options
  _options = o;
  _debounce.options = o;
  
  //setup GUI for parameters
  _initGui();
  
  //calculate
  _calculate();

  //make mesh
  _makeMesh();
  
  //Make sure wave table is set to size
  _waveTable.resize(_size);
  
  //Update the wavetable
  _updateWaveTable(true);
  
  //Add keyboard event listenters
  ofAddListener(ofEvents().update, this, &OctaveNoise::update);
  ofAddListener(ofEvents().keyPressed, this, &OctaveNoise::keyPressed);
  
}

void OctaveNoise::update(ofEventArgs& args) {
  
  if (_checkDebounce()) {
    updateNoise();
  }
  
}

#pragma mark - Getters / Setters

WaveTableOsc& OctaveNoise::osc() {
  
  return _waveTableOsc;
  
}


void OctaveNoise::updateNoise() {

  //calculate
  _calculate();

  //make mesh
  _updateMesh();
  
  //update the wavetable
  _updateWaveTable();
  
}


void OctaveNoise::updateNoise(Options o) {
  
  //Setup options
  _options = o;
  updateNoise();
  
  
}




#pragma mark - Calculations

void OctaveNoise::_calculate() {
  
  //This is the main calculation for the octave noise to generate the terrain, which we will then extract
  //the wave table from
  
  //Just for interest, calculation time check
  int64_t start = ofGetElapsedTimeMillis();
  
  cout << "started wavetable calc at " << start << "ms" << endl;
  
  _n_samples = _size * _size;
  _samples.clear();
  _samples.resize(_n_samples);
  
  //Seed a random value (will let us save positions and recreate them)
  ofSeedRandom(_options.seed);
  
  //Build octave offsets - i.e. for each additional octave value we add in the main loop, we will
  //take a perlin noise value from a different offset than the ones preceding it
  _Offsets _offsets;
  _offsets.resize(_options.octaves);
  for (_Offsets::iterator it = _offsets.begin(); it != _offsets.end(); ++it) {
    
    (*it) = glm::vec2(ofRandom(-OFFSET_RANGE, OFFSET_RANGE), ofRandom(-OFFSET_RANGE, OFFSET_RANGE));
    (*it) += _options.offset;
        
  }
  
  //Make sure scale is not zero (avoid divide by zero errors)
  if (_options.scale <= 0.0f) {
    _options.scale = EPSILON;
  }
  
  //Setup min/max values so we can normalise the data afterwards
  MinMax bounds = {
    .min = numeric_limits<float>::max(),
    .max = numeric_limits<float>::min()
  };

 
  int i = 0;
  
  for (_Samples::iterator sit = _samples.begin(); sit != _samples.end(); ++sit, i++) {
      
    float amplitude = 1.0f;
    float frequency = 0.4f;
    
    (*sit).x = i % _size;
    (*sit).z = i / _size;
    (*sit).y = 0.0f;
    
    int oct = 0;
    for (_Offsets::iterator it = _offsets.begin(); it != _offsets.end(); ++it, oct++) {
      
      glm::vec2 p = glm::vec2((*sit).x, (*sit).z);
      p *= (_options.scale * frequency);
      p += (*it);
      (*sit).y += (ofSignedNoise(p) * amplitude);
      
      if ((*sit).x == 1 && oct == 0) {
        cout << p << " - " << (*sit) << endl;
      }
      
      amplitude *= _options.persistance;
      frequency *= _options.lacunarity;
   
      
    }
  
    //Update bounds
    if ((*sit).y > bounds.max) {
      bounds.max = (*sit).y;
    }
    if ((*sit).y < bounds.min) {
      bounds.min = (*sit).y;
    }

    
  }
  
  //Normlaise values to -1.0f > 1.0f
  for (_Samples::iterator sit = _samples.begin(); sit != _samples.end(); ++sit) {
    (*sit).y = ofMap((*sit).y, bounds.min, bounds.max, -1.0f, 1.0f, true);
  }
  
  //Get the run time and let us know
  int64_t runtime = ofGetElapsedTimeMillis() - start;
  cout << "generated " << _n_samples << " for " << _options.octaves << " ocatves in " << runtime << "ms" << endl;
  
}


#pragma mark - WaveTable

//Using normliased position we grab a wavtable from the _Samples vector - I'm not sure I need to specific
//version of this for no extrapolation - it was useful however in first creating the extract of the wave table
//in reality I should refactor this out I think

//See observations for comments about interpoaltion, and smmothing of the wavetable

void OctaveNoise::_updateWaveTable(bool initMesh) {
  
  //Do we need to initialise the mesh?
  if (initMesh) {
    _Samples s;
    s.resize(_size);
    _waveTableMesh.addVertices(s);
    vector<ofFloatColor> c;
    c.resize(_size,ofFloatColor::red);
    _waveTableMesh.addColors(c);
    _waveTableMesh.setMode(OF_PRIMITIVE_LINE_STRIP);
  }

  _waveTableForm.clear();
  
  
  //We have a normlaised position in the wavetable which we need to convert to an absolute position
  //based on size
  
  float fp = (_size - 1) * _waveTablePosition;
  float fd = fp - floor(fp);
  
  cout << _waveTablePosition << " " << fp << ", " << fd << " ";
  
  if (fd == 0.0f) {
    int p = fp;
    _extractWaveTable(p);
  } else {
    int p1 = fp;
    int p2 = fp + 1;
    cout << p1 << ", " << p2 << endl;
    _extractWaveTable(p1, p2, fd);
  }
  
  _waveTableOsc.set(_waveTable);
  
}

void OctaveNoise::_extractWaveTable(int p) {

  //No interpolation required, but we need to setup correctly for the direction
  size_t s = (_waveTableDirection == NORTH_SOUTH) ? (p * _size) : p;

  for (int i = 0; i < _size; i++) {
    size_t ii = (_waveTableDirection == NORTH_SOUTH) ? (s + i) : p + ( i * _size);
    _waveTable[i] = _samples[ii].y;
    _waveTableMesh.setVertex(i, _samples[ii]);
  }

  _smoothWaveTable();

  for (int i = 0; i < _size; i++) {
   _waveTableForm.addVertex(glm::vec3(i, _waveTable[i], 0.0f));
  }

  
}

void OctaveNoise::_extractWaveTable(int p1, int p2, float d) {

  //Slightly more complex, as we need to extrapolate between p1 and p2
  size_t s1 = (_waveTableDirection == NORTH_SOUTH) ? (p1 * _size) : p1;
  size_t s2 = (_waveTableDirection == NORTH_SOUTH) ? (p2 * _size) : p2;

  for (int i = 0; i < _size; i++) {
    size_t ii1 = (_waveTableDirection == NORTH_SOUTH) ? (s1 + i) : p1 + ( i * _size);
    size_t ii2 = (_waveTableDirection == NORTH_SOUTH) ? (s2 + i) : p2 + ( i * _size);
        
    glm::vec3 lerp = _samples[ii1] + (d * (_samples[ii2] - _samples[ii1]));
    
    _waveTable[i] = lerp.y;
    _waveTableMesh.setVertex(i, lerp);
    
  }

  _smoothWaveTable();

  for (int i = 0; i < _size; i++) {
   _waveTableForm.addVertex(glm::vec3(i, _waveTable[i], 0.0f));
  }

}

void OctaveNoise::_smoothWaveTable() {
  
  //Called to fix start and ends so they are within _waveTableGapMax, over _smoothSamplesRange
  if (_waveTableSmooth) {
    //Only do if have been setup to do
    double _diff = _waveTable[0] - _waveTable[_size - 1];
    if (abs(_diff) < _smoothSamplesRange) {
      //Should look up more techniques for handling this - but lets try the 4 point interpoaltion
      //approach to set the first and last values - using the last / first two samples of the
      //extracted wave table
      double v = WaveTableOsc::interpolate4(_waveTable[_size-3], _waveTable[_size-2], _waveTable[1], _waveTable[2], 0.0f);
      _waveTable[_size - 1] = v;
      _waveTable[0] = v;
     }
  }
}



#pragma mark - Mesh creation and update, fairly straightforward as we work with the values from the terrain generated

//There is probably a duplication of data here, as we have _Samples as an array of vec3 vertices, and then duplicate
//into the mesh object. Should move to a VBO, which makes using an existing array / vector of values easier to allocate
//straight to the mesh (and of course is now based on the GPU, and as we are not updating frequently, avoids some of the
//downsides of a VBO)

void OctaveNoise::_makeMesh() {
  
  //set all colours to base color
  _colors.resize(_n_samples, ofFloatColor::aliceBlue);
    
  _mesh.setMode(OF_PRIMITIVE_TRIANGLES);
  _mesh.addVertices(_samples);
  _mesh.addColors(_colors);
  
  //Now need to make triangles
  for (int y = 0; y < (_size - 1); y++) {
    for (int x = 0; x < (_size - 1); x++) {
      
      ofIndexType i = y * _size + x;
      
      _mesh.addIndices({ i, i + 1, static_cast<unsigned int>(i + _size)});
      _mesh.addIndices({ i + 1, static_cast<unsigned int>(i + _size + 1), static_cast<unsigned int>(i + _size)});
      
    }
  }
  
  
  
}

void OctaveNoise::_updateMesh() {
  
  ofIndexType i = 0;
  for (_Samples::iterator it = _samples.begin(); it != _samples.end(); ++it, i++) {
    _mesh.setVertex(i, (*it));
  }
  
}





#pragma mark - Drawing

void OctaveNoise::drawWave(ofRectangle r) {
  
  //Draws the wave in 2D in the specified rectangle
  
  ofPushMatrix();
  
  ofTranslate(r.position);
  
  ofFill();
  ofSetColor(ofColor(5,5,5,180));
  //Draw Background
  ofDrawRectangle(0, 0, r.width, r.height);
  
  //Draw wavetable polyline - we need to figure out scale
  ofPushMatrix();
  ofTranslate(0.0f, r.height / 2.0f);

  ofScale(r.width / (_size - 1), r.height / 2.0f);
  
  ofNoFill();
  ofSetColor(ofColor::red);
  
  _waveTableForm.draw();
  
  ofPopMatrix();
  
  //DrawFrame
  ofNoFill();
  ofSetColor(ofColor::white);
  ofDrawRectangle(0, 0, r.width, r.height);

  
  ofPopMatrix();
  
}


void OctaveNoise::drawMesh() {
 
  //Draws the mesh, pretty straightforward really - also draws a single line copy of the wavetable for
  //originally this was just a change of colour in the terrain mesh, but of couse this is interpolated between
  //points, and so the ones either side started to pick up some red colouring, and create a "fat" stripe
  
   float t = (_size / 2.0f) * _scale.x;
  
  ofTranslate(-t, 0.0f, -t);
  ofScale(_scale);
  _mesh.drawWireframe();
  
  _waveTableMesh.drawWireframe();
  
}





#pragma mark - GUI

void OctaveNoise::_initGui() {
  
  //Setup an ofxDatGui set of controls - I'd oriignally gone with this, as there some really useful
  //components such as the Value Plotter and Waveform Monitor, [link](http://braitsch.github.io/ofxDatGui/components.html#value-plotters)
  //which I didn't get a chance to implement
  
  //See notes in the .h file about GUI debounce, in the end, at a size of 512 for the terrain, generation times
  //are around 10ms - however I did start with larger sizes, and much longer generation times, and this can
  //be problematic when the sliders generate multiple events
  
  _gui = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
  _gui->addHeader("NoiseWavetables v1.0");
  _gui->addFooter();
  
  //Noise Generation Parameters
  ofxDatGuiFolder* f = _gui->addFolder("Noise Gen Parameters");
  //Seed
  ofxDatGuiTextInput* seed = f->addTextInput("seed", ofToString(_options.seed));
  seed->onTextInputEvent([&](ofxDatGuiTextInputEvent e){
    //handle for text input
    int _seed = ofToInt(e.text);
    if (_seed == 0) {
      //looks like crappy input
      seed->setText(ofToString(_options.seed));
    } else {
      _options.seed = _seed;
      updateNoise();
    }
  });
  
  //New random seed
  ofxDatGuiButton* randomSeed = f->addButton("new random seed");
  randomSeed->onButtonEvent([=](ofxDatGuiButtonEvent e) {
    int _seed = ofRandom(SEED_MAX);
    seed->setText(ofToString(_seed));
    _options.seed = _seed;
    updateNoise();
  });

  //Scale
  ofxDatGuiSlider* scale = f->addSlider("scale", 0.001, 1.0, _options.scale);
  scale->setPrecision(3);
  scale->onSliderEvent([&](ofxDatGuiSliderEvent e){
    _debounce.options.scale = e.value;
    _initDebounce();
  });
  
  
  
  //Octaves
  ofxDatGuiSlider* octaves = f->addSlider("n.octaves", 1, 50, _options.octaves);
  octaves->setPrecision(0);
  octaves->onSliderEvent([&](ofxDatGuiSliderEvent e){
    _debounce.options.octaves = e.value;
    _initDebounce();
  });

  
  //Persistance
  ofxDatGuiSlider* persistance = f->addSlider("persistance", 0.001, 5.0, _options.persistance);
  persistance->setScale(2);
  persistance->onSliderEvent([&](ofxDatGuiSliderEvent e){
    _debounce.options.persistance = e.value;
    _initDebounce();
  });

  
  
  
  //Lacunarity
  ofxDatGuiSlider* lacunarity = f->addSlider("lacunarity", 0.001, 2.0, _options.lacunarity);
  lacunarity->setScale(2);
  lacunarity->onSliderEvent([&](ofxDatGuiSliderEvent e){
    _debounce.options.lacunarity = e.value;
    _initDebounce();
  });
  
  
  
  //Offset
  ofxDatGui2dPad* offset = f->add2dPad("offset");
  offset->setBounds(ofRectangle(0,0,50,50));
  offset->setPoint(_options.offset);
  offset->on2dPadEvent([&](ofxDatGui2dPadEvent e) {
    _debounce.options.offset = glm::vec2(e.x, e.y);
    _initDebounce();
  });
  
  
  f->expand();
  
  ofxDatGuiFolder* ff = _gui->addFolder("wavetable parameters");
  
  _wtPosSlider = ff->addSlider("position", 0.0, 1.0, _waveTablePosition);
  _wtPosSlider->setPrecision(3);
  _wtPosSlider->onSliderEvent([&](ofxDatGuiSliderEvent e) {
    //we don't need to debounce this input, as extracting wavetable should be fast
    _waveTablePosition = e.value;
    _updateWaveTable();
  });
  
  ofxDatGuiButton* wtDirection = ff->addButton("NORTH_SOUTH");
  wtDirection->onButtonEvent([&](ofxDatGuiButtonEvent e) {
    //we are just toggling direction here
    if (_waveTableDirection == NORTH_SOUTH) {
      e.target->setLabel("EAST_WEST");
      _waveTableDirection = EAST_WEST;
    } else {
      e.target->setLabel("NORTH_SOUTH");
      _waveTableDirection = NORTH_SOUTH;
    }
    _updateWaveTable();
  });
  
  
  ff->expand();
  
  
}

#pragma mark GUI debouncing

void OctaveNoise::_initDebounce() {
  _debounce.check = true;
  _debounce.at = ofGetFrameNum() + DEBOUNCE_FRAMES;
}

bool OctaveNoise::_checkDebounce() {
  
  bool r = _debounce.check && ofGetFrameNum() > _debounce.at;
  if (r) {
    _options = _debounce.options;
    _debounce.check = false;
    _debounce.at = 0;
  }
  return r;
  
}



bool OctaveNoise::mouseOverGui(int x, int y) {
  
  //test to see if mouse position is within bounds of the GUI
  ofRectangle bounds = ofRectangle(_gui->getPosition(), _gui->getWidth(), _gui->getHeight());
  return bounds.inside(x, y);
  
}


#pragma mark - Handle other inputs, such as keyboard

void OctaveNoise::keyPressed(ofKeyEventArgs &args) {
  
  if (args.key == '-') {
    _waveTablePosition -= WAVETABLE_NUDGE;
    if (_waveTablePosition < 0.0f) {
      _waveTablePosition = 0.0f;
    }
    _wtPosSlider->setValue(_waveTablePosition);
  }

  if (args.key == '_') {
    _waveTablePosition -= WAVETABLE_SHIFT_NUDGE;
    if (_waveTablePosition < 0.0f) {
      _waveTablePosition = 0.0f;
    }
    _wtPosSlider->setValue(_waveTablePosition);
  }
  
  if (args.key == '=') {
    _waveTablePosition += WAVETABLE_NUDGE;
    if (_waveTablePosition > 1.0f) {
      _waveTablePosition = 1.0f;
    }
    _wtPosSlider->setValue(_waveTablePosition);
  }

  if (args.key == '+') {
    _waveTablePosition += WAVETABLE_SHIFT_NUDGE;
    if (_waveTablePosition > 1.0f) {
      _waveTablePosition = 1.0f;
    }
    _wtPosSlider->setValue(_waveTablePosition);
  }

  
}
