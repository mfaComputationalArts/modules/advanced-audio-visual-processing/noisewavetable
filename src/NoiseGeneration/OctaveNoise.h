/**
 
 Class: OctaveNoise
 
 Used to create a wavetablke using 2D perlin noise in octaves, for this simple demo, we will create a 2D "landscape"
 which is in effect N samples wide, and then has N variants in the y dimension so that we can pick and playback
 a slice of the landscape.
 
 We need to be able to do the following in the class
 
 1. generate the set of points which are n^2 using inputs for seed, scale, octaves, persistance, lacunarity, offset(v2)
 2. extract these points as a wave table for use in maximilian - picking a y dimension variant
 3. regenerate as and when required
 4. draw the current landscape, and the currently selected wave table
 
 The content of this is inspired by
 
 https://www.youtube.com/watch?v=MRNFcywkUSA
 https://github.com/SebLague/Procedural-Landmass-Generation/blob/master/Proc%20Gen%20E03/Assets/Scripts/Noise.cs
 
 */


#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add on includes
#include "ofxDatGui.h"

//Local includes
#include "WaveTableOsc.h"




class OctaveNoise {
    
public:
    
  OctaveNoise(size_t n = 128) : _size(n) {};
   
  const int SEED_MAX = 27072;
  
  struct Options {
    int seed;
    float scale;
    int octaves;
    float persistance;
    float lacunarity;
    glm::vec2 offset;
    size_t sampleRate;
  };
  
  void setup(Options o);
  
  void update(ofEventArgs& args);

  WaveTableOsc& osc();               //Return a reference to the wavetable oscilator
  
  void updateNoise();
  void updateNoise(Options o);
  
  void drawWave(ofRectangle r);
  void drawMesh();
  
  bool mouseOverGui(int x, int y);
  
  //Event Listeners
  void keyPressed(ofKeyEventArgs& args);
  
  
private:
    
  size_t _size;
  size_t _n_samples;
  
  //Options for calculation
  Options _options;
  
  typedef vector<glm::vec2> _Offsets;
  typedef vector<glm::vec3> _Samples;
  _Samples _samples;
  
  //Noise calculation
  void _calculate();
  
  const float OFFSET_RANGE = 173.098f;
  const float EPSILON = 0.0001f;
  
  struct MinMax {
    float min;
    float max;
  };
  
  //Mesh for drawing current view
  ofMesh _mesh;
  vector<ofFloatColor> _colors;
  
  void _makeMesh();
  void _updateMesh();
    
  glm::vec3 _scale = glm::vec3(15.0f, 60.0f, 15.0f);
    
    
  /**
   Wavetable stuff
   
   We will have an vector of values (length size) that represents the current wavetable - this will be
   defined by;
   
   * a direction
   * a normalised position
   
   
   we will then interpolate from the noise landscape the values that make up that wavetable
   
   we will also need to ensure the start and end of the wavetable have a close enough match in
   value, as extreme differences would lead to cliking sounds as the wavetable loops
   
   Future enhacements would be to allow abstract linear directions, and path following over the
   landscape to extract the sample
   
   */
  
  enum WaveTableDirection {
    NORTH_SOUTH,
    EAST_WEST
  };
  
  WaveTableDirection _waveTableDirection = NORTH_SOUTH;
  double _waveTablePosition = 0.5;
  
  WaveTableOsc::WaveTable _waveTable;
  
  ofMesh _waveTableMesh;                //Mesh used to draw extraced wavetable
  ofPolyline _waveTableForm;            //Used to draw flat outline at topleft of screen
  
  bool _waveTableSmooth = true;         //Should we correct the start and end of the wavetable
  double _waveTableGapMax = 0.1;        //Max permitted gap between end and start of wave table
  int _smoothSamplesRange = 10;          //Number of samples to adjust at begining and end of wavetable
  
  void _updateWaveTable(bool initMesh = false);  //Extract wave table currently specified in parameters

  void _extractWaveTable(int p);
  void _extractWaveTable(int p1, int p2, float d);
  
  void _smoothWaveTable();
  
  const float WAVETABLE_NUDGE = 0.001f;
  const float WAVETABLE_SHIFT_NUDGE = 0.01f;
  
  WaveTableOsc _waveTableOsc;
  
  /**
   
   GUI for parameter control
  
   
   Debounce GUI input - have found issues with expensive operations that occur as a result of parameter
   changes from the GUI this can get messy with multiple events triggered. We'll set a debounce number
   of frames, in which data input should be completly static before carrying out the update of the
   noise data.

   */
  

  ofxDatGui* _gui;
  void _initGui();
  
  ofxDatGuiSlider* _wtPosSlider;
  
  const uint64_t DEBOUNCE_FRAMES = 5;       //Number of frames in which we see static input
  
  
  struct {
    bool check;
    uint64_t at;
    Options options;
  } _debounce;

  void _initDebounce();
  bool _checkDebounce();
  
  
  
};

