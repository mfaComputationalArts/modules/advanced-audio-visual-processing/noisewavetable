#include "ofApp.h"

void ofApp::setup(){

  OctaveNoise::Options o;
  
  o.seed = 90125;
  o.scale = 0.171f;
  o.octaves = 4;
  o.persistance = 0.73;
  o.lacunarity = 1.01;
  o.offset = glm::vec2(0.0f);
  o.sampleRate = SAMPLE_RATE;
  
  _noise.setup(o);
  
  
  
  //Camera setup
  _cam.setFarClip(-500.0f);
  //camera pos: 1433.1, 1182.78, 1506.5 lookatDir: -0.59909, -0.494446, -0.629773
  _cam.setPosition(1433.1, 1182.78, 1506.5);
  _cam.lookAt(glm::vec3(0.59909, -0.494446, -0.629773));

  //Sound stream - do this last just to make sure that we have the initial waveTable built
  _setupSoundStream();

}

void ofApp::update(){

}

void ofApp::draw(){

  _cam.begin();
  
  ofPushMatrix();
  
  
  _noise.drawMesh();
  
  ofPopMatrix();
  
  _cam.end();
  
  //Draw HUD, GUI etc type stuff
  _noise.drawWave(_waveRect);
  
}

#pragma mark - Soundstream setup and callbacks

void ofApp::_setupSoundStream() {
  
  //Try to setup the soundstream based on the specifed device name - if we can't find
  //it then we will stop the application
  
  //Get a list of all the devices available
  vector<ofSoundDevice> devices = _soundStream.getDeviceList();
  
  vector<ofSoundDevice>::iterator it = find_if(devices.begin(), devices.end(), [&](const ofSoundDevice& d){
    return d.name == DEVICE_NAME;
  });
  
  if (it == devices.end()) {

    cout << "Could not find device " << DEVICE_NAME << " check loop back is running and virtual device has the right name" << endl;

  } else {
    cout << "Found device " << DEVICE_NAME << " will connect and setup" << endl;

    ofSoundStreamSettings settings;
    settings.setOutDevice(*(it));
    settings.setOutListener(this);
    settings.sampleRate = SAMPLE_RATE;
    settings.numOutputChannels = NUMBER_OUT_CHANNELS;
    settings.bufferSize = BUFFER_SIZE;
    
    _soundStream.setup(settings);

  }
  
  
}

void ofApp::audioOut(ofSoundBuffer& output) {
  
  double frequency;
  
  if (_playing > -1) {
    frequency = _midiNoteToFrequency(_playing);
  }
    
  for (size_t i = 0; i < BUFFER_SIZE; i++) {
    
    double v = 0.0;
    
    size_t ii = i * NUMBER_OUT_CHANNELS;
    
    if (_playing > -1) {
      //get osc values from the noise wave tables
      v = _noise.osc().getSample(frequency) * 0.75;
    }
    //set the buffer output
    output[ii] = v;
    output[ii + 1] = v;
  
  }
  
}



#pragma mark - Computer MIDI Keyboard, in style of ableton live

void ofApp::keyPressed(int key) {
  
  if (_noteMap.count(key) > 0) {
    _noteDown(key);
  }
 
  //Handle modifier for keyboard range
  if (_modifierMap.count(key) > 0) {
    _currOctave += _modifierMap[key];
    if (_currOctave < 1) {
      _currOctave = 1;
    }
    if (_currOctave > 9) {
      _currOctave = 9;
    }
  }
  
}

void ofApp::keyReleased(int key) {
  
  //We only care about notes on release
  if (_noteMap.count(key) > 0) {
    _noteUp(key);
  }
  
}

void ofApp::_noteDown(int key) {
  
  _playing = _midiNoteFromKeyboardInput(_noteMap[key]);
  
}

void ofApp::_noteUp(int key) {
  
  //re observation 1. - check that the note Up/Off is actually the one being played
  
  if (_midiNoteFromKeyboardInput(_noteMap[key]) == _playing) {
    _playing = -1;
  }
  
}

int ofApp::_midiNoteFromKeyboardInput(int i) {
  
  //C0 is midi note 24 (treating as bottom of range)
  return 12 + (_currOctave * 12) + i;
  
}



void ofApp::mouseMoved(int x, int y) {
  
  //Enable / Disable mouse input accordingly if mouse is over the GUI or not
  if (_noise.mouseOverGui(x, y)) {
    _cam.disableMouseInput();
  } else {
    _cam.enableMouseInput();
  }
  
}



double ofApp::_midiNoteToFrequency(int note) {
  
  //Convert a MIDI note into a frequency, so we can play the wavetable
  //taken from https://www.music.mcgill.ca/~gary/307/week1/node28.html
  
  const double aFreq = 440;
  
  return (aFreq / 32) * pow(2, ((note - 9) / 12.0));
  
}
