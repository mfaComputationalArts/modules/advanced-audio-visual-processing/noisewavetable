/**
 
 # Advanced Audio Visual Processing - Courseworks 02
 
 ## Terrain Synth, noise generated terrain as source for wave tables
 
 ### Nathan Adams / nadam001@gold.ac.uk
 
 Experiment to do the following
 
 1. Create a terrain using an octave noise approach
 2. Use slices of the terrain as a wave table
 
 In looking at how to implment a noise octave approach to terrain generation, this work is primarily
 based on this [video](https://www.youtube.com/watch?v=MRNFcywkUSA) by Sebastian Legue, and his source
 code for Unity / C# which can be found on [github](https://github.com/SebLague/Procedural-Landmass-Generation/blob/master/Proc%20Gen%20E03/Assets/Scripts/Noise.cs)
 
 Wavtable playback is monophonic
 
 Files / Structure
 
 * ofApp.h / .cpp - main app, handles setup of other objects, and output of audio, and input of MIDI notes via the computer keyboard
 * NoiseGeneration/OctaveNoise.h / .cpp - OctaveNoise generation, and extraction of wavetables from the terrain
 * WaveTable/WaveTableOsc.h / .cpp - based on maxiOsc::sinbuf4, a wave table oscilator which returns the current sample in the same way other maxiOsc objects do.
 
 Other Files
 
 * bin/data/ofxbraisch - support files for the ofxDatGui add on, which I prefer in certain cases over the standard ofxGui
 
 
 ## Instructions
 
 Once application is launched, the noise terrain can be modified using the GUI in the top right of the window, allowing control
 
 * Random seed - each octave of noise is offset by a 2D vector, the position of this is determined using ofRandom, and random seed allows for the ability to recreate a specific form. There is also a new button to generate a new random value for the seed value
 * Scale - this is the main scaling value for the step between values the larger it is, the noiser the landscape will become, smaller values will create a more undulating or flat terrain
 * Octaves - number of octaves, i.e. how many sets of data are added together, with changing frequncy of value, thus higher octaves add more detail
 * Persistance - this determines how the amplitude of each octave changes - values of less than 1 will cause each octave to add slightly less, whereas greater than one will cause each octave to have a larger amplitude
 * Lacunarity - similar persistance, accept this changes how the frequency is modified in each octave - this should ideally be greater than one, as we want an increasing scale, so that smaller details are a little noiser and help create a more fractal like terrain
 * Offset - this is a 2D vector, that controls the main offset used in each octave (i.e. how much is added to the inputs to the signed perlin noise function)
 
 Wavetable controls
 
 * Position - a normalised value between 0 and 1, which controls which slice of the terrain we take as a wavetable
 * Direction (NORTH_SOUCE / EAST_WEST) - controls which direction the slice is taken
 
 Keyboard Controls
 
 * Wave Table position - +/- keys nudge the position by 0.001 in either direction, use the shift key to change this to 0.01 per key press
 
 * Octave - Z/X raise lower octave of notes played
 * Play Notes - ASDFGHJK - place notes C/D/E/F/G/A/B/C and WETYUI are the incidentals
 
 
 ## Observations / Future Steps
 
 1. Note on/off behaviour is a little odd - I think if you try to play a fairly legato style betwen notes, the second one won't often sound, which I think is because the note on for the next note, is triggered before the note off of the first one. This could be fixed by making note off check if the key pressed is the note currently being played. (This change applied as actually a quick one)
 
 2. Sounds are a little buzzy, espercailly when greater scale / octave values are added - this does create interesting FM like sounds, but isn't quite working in an ideal way, especially when changing the wave table position in the terrain. There are two things that would be good to look at
 
 2a. Using the 4 point interpolation from the oscialtor / sinbuf4 for a better interpolation of the wave table in the original position (interpolate4 has been made a static function to allow for this sort of useage)
 
 2b. I think the apprach I'm using to treat the start / end of the sample so that there are not any large steps in value is naive, and should work over more points than just the end point. As an interesting diversion here, looking at using three dimensional noise might help create loops of noise behaviour as seen in this [video](https://www.youtube.com/watch?v=ZI1dmHv3MeM) by Daniel Shiffman.
 
 3. Move to polyphonic playback would be a good exercise to go though
 
 4. I'd like to try turning this into a plugin, via JUCE or similar framework - would be interesting to think about how to tackle the graphics side of this, and have a nice 3D display
 
 5. Non-linear / orthogonal paths - it would be good to be able to see what happens if you take the wavetable from a variety of different paths over the terrain, see point 6.
 
 6. This eventually should become the basis for a more complex sound environemnt I'm working on - as part of it, sounds will be controlled through the use of boids flying through a landscape. I'd like to use the track of the boid over the landscape to extract a wave table which is used for the sound - thus meaning each boid has it's own sound, that should also live in a unified space based on the terrain

 7. ofxMaxim is specified as an add on in `addons.make` though in the end I haven't actually used, as couldn't see any obvious functionality for wavetables (and in the end it was useful to explore how sinbuf4 works and try for myself!)
 
 */

#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add on includes

//Local Includes
#include "OctaveNoise.h"

class ofApp : public ofBaseApp{
  
public:
  void setup();
  void update();
  void draw();
  
  //Openframeworks events
  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y);
  
  //Audio callback(s)
  void audioOut(ofSoundBuffer& output);

private:
  
  
  
  //Camera for 3D view of Noise Mesh
  ofEasyCam _cam;

  //Rectanghle to define where the wavetable preview is drawn
  ofRectangle _waveRect = ofRectangle(10,10, 1300, 200);

  //Audio settings
  //Use output to audio interface on USB
  //const string DEVICE_NAME = "Focusrite: Scarlett 2i4 USB";
  const string DEVICE_NAME = "Rogue Amoeba Software, Inc.: Loopback Audio"; //loopback for audio recording
  
  //Device settings
  const size_t SAMPLE_RATE = 48000;
  const size_t NUMBER_OUT_CHANNELS = 2;
  const size_t BUFFER_SIZE = 64;
  
  const size_t BUFFER_N = NUMBER_OUT_CHANNELS * BUFFER_SIZE;
  
  ofSoundStream _soundStream;
  void _setupSoundStream();
  
  
  //OctaveNoise which is used to generate the octave noise map and associated wavetables
  OctaveNoise _noise;

  
  
  /**
   
   Computer MIDI Keyboard:
   
   Same style as the Ableton Live one, where Z/X control octave, and the ASDF row of keys are the white
   notes, and QWER… are the black notes
   
   */
  typedef map<int, int> KeyMappings;
  
  //Key mappings, use the same as abletons Computer MIDI keyboard
  KeyMappings _modifierMap = {
    //Octave Modifiers
    { 'z', - 1 },
    { 'x', + 1 }
  };
  
  KeyMappings _noteMap = {
    //Actual Keys
    { 'a', 0 },                          //C
    { 'w', 1 },                          //C# / D♭
    { 's', 2 },                          //D
    { 'e', 3 },                          //D# / E♭
    { 'd', 4 },                          //E
    { 'f', 5 },                          //F
    { 't', 6 },                          //F# / G♭
    { 'g', 7 },                          //G
    { 'y', 8 },                          //G# / A♭
    { 'h', 9 },                          //A
    { 'u', 10 },                         //A# / B♭
    { 'j', 11 },                         //B
    { 'k', 12 }                          //C
  };
  
  double _midiNoteToFrequency(int note);
  
  void _noteDown(int key);
  void _noteUp(int key);
  
  int _midiNoteFromKeyboardInput(int i);
  
  int _currOctave = 4;                   //So we start a C4 - middle C
  
  //Keeping monophonic for this item - be good to look at polyphony, and diff ways of hanndling multiple
  //note down / ups
  
  int _playing = -1;                     //Currently playing MIDI note, -1 for not playing
  
};
